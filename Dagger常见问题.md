## Dagger常见问题

#### 一、Dagger2的描述

Dagger本是Squar出的一款第三方框架，本意是像ButterKnife通过注解的方式来提供对象的注入，现在对于目前的版本，衍生的产品也有很多，比如Google辅助出的Dagger2，亲儿子Hilt，这些都是底层都是通过Dagger来实现的。

但是Dagger的入门难度很大，对于不是组件化的项目而言，Dagger的使用意义不大，并且还会伴生出很多模板代码，虽然Dagger2已经有很大的改善，但是依然无法改变目前尴尬的地位，有些鸡肋的意味，食之无味，弃之可惜。

尽管Dagger2有这样或者那样的问题，对于开发者而言，项目随着运营和需求的迭代，组件化、模块化大势所趋，为了管理对象的生命周期，避免内存泄漏等问题，Dagger2虽然可能不是终点，但是肯定是解决的一条途径。

#### 二、Dagger2的相关基础内容

Dagger的爽文推荐：

- [听说你还不会用Dagger2？Dagger2 For Android最佳实践教程](https://juejin.cn/post/6844903696417947662)
- [Android基础知识：Dagger2入门](https://juejin.cn/post/6844903840450347016)
- [Dagger2华丽使用在MVP框架中](https://juejin.cn/post/6844903935929499662)
- [基于MVP+RxJava2+Retrofit+Dagger2+MD的仿B站Android客户端](https://juejin.cn/post/6844903575622008846)

#### 三、Dagger2在使用过程中一些问题

##### 1.@Component.Builder is missing setters for required modules or components: [xxx.xxx.xxx.AppComponent]

这个问题目前是在子类Component依赖于AppComponent之后，即：dependencies = AppComponent.class，这个需要在子类Component的Builder接口中，实现和AppComponent的绑定；

``` java
@ActivityScope
@Component(modules = SplashModule.class, dependencies = AppComponent.class)
public interface SplashComponent {

    void inject(SplashActivity activity);

    BaseRecyclerViewAdapter<NewsBean> getAdapter();


    @Component.Builder
    interface Builder {
        SplashComponent.Builder setAppComponent(AppComponent component);

        @BindsInstance
        SplashComponent.Builder view(SplashActivity activity);

        SplashComponent build();
    }
}
```

##### 2. @Binds作用

例如在MVP架构下使用Dagger2，需要在Presenter中注入View层接口的实现类，那么这个注解便能起到作用

比如：SplashActivity是SplashContract.View层的实现类，在SplashPresenter层中持有SplashContract.View的实现类，这个是无法直接注入SplashActivity，必须得注入SplashContract.View，但是可以在SplashModule中，构造抽象方法来实现SplashActivity和SplashContract.View的绑定，避免在SplashComponent实现SplashContract.View和SplashActivity两位的注入

``` java
@Module
public abstract class SplashModule {

    @ActivityScope
    @Provides
    public static BaseRecyclerViewAdapter<NewsBean> providerAdapter(SplashContract.View view) {
        BaseRecyclerViewAdapter<NewsBean> adapter = new BaseRecyclerViewAdapter<NewsBean>(view.getDependencies(),
                R.layout.layout_adapter_news) {
            @Override
            protected void convert(int position, @Nullable BaseViewHolder holder, @Nullable NewsBean data) {
                holder.setImageRoundResource(R.id.img, data.getThumbnail_pic_s(), R.dimen.dp_6)
                        .setText(R.id.tv_title, data.getTitle())
                        .setText(R.id.tv_author, data.getCategory())
                        .setText(R.id.tv_time, data.getDate());
            }
        };
        return adapter;
    }
    
    @Binds
    abstract SplashContract.View bindView(SplashActivity activity);
}
```





